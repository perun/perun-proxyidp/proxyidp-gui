from enum import Enum


class NotificationType(Enum):
    VERIFICATION = 1
    CONFIRMATION = 2
