class InvalidJWTError(Exception):
    pass


class UserNotExistsException(Exception):
    pass
