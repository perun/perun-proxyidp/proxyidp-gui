## [8.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.2.1...v8.2.2) (2024-10-21)


### Bug Fixes

* heuristic queries left outer join instead just left ([026cc2d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/026cc2da671ba600c85b38fbe3e5f3e1b33899f7))

## [8.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.2.0...v8.2.1) (2024-09-25)


### Bug Fixes

* saml logout ([d7b4860](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d7b48605cd1ff42b22203dc13283a1b4d5c850ab))

# [8.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.1.3...v8.2.0) (2024-09-24)


### Features

* sqlalchemy for flask-sessions ([07ba53f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/07ba53f7745781628f3f7f034c58fbd64d902e1c))

## [8.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.1.2...v8.1.3) (2024-09-03)


### Bug Fixes

* use engine.begin() instead of connect() ([1c53502](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/1c535024eceabc40cb0d6c0d561aaeb79b48cea2))

## [8.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.1.1...v8.1.2) (2024-08-08)


### Bug Fixes

* handle unknown user error ([86e23ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/86e23ad6e46a43515cf7ffba27b7cc1e54bded8c))

## [8.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.1.0...v8.1.1) (2024-08-07)


### Bug Fixes

* catch error on nonexistent domain name ([a834a69](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/a834a69c5f47f15ad37e716c3de85f4eabdb9fed))

# [8.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v8.0.0...v8.1.0) (2024-07-29)


### Features

* remove satosa from user_manager ([a988486](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/a9884868abc0bd9547fcc234c9615cb0186b2f5a))

# [8.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.10...v8.0.0) (2024-07-10)


### Features

* logout system redesign ([993e910](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/993e910e9790f54e41e65f087e768ca5985c2f2d))


### BREAKING CHANGES

* new required attributes under `logout` in `perun.proxygui.yaml`

## [7.2.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.9...v7.2.10) (2024-06-18)


### Bug Fixes

* edit reset MFA request message ([77d6d1a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/77d6d1a9a6bd523ed698360589fe6fc511d3a5de))

## [7.2.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.8...v7.2.9) (2024-06-03)


### Bug Fixes

* enforce MFA on correct endpoints ([1ea7e2e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/1ea7e2ea8c6ac408eb0c14fc5cdd17013e6b2677))

## [7.2.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.7...v7.2.8) (2024-06-03)


### Bug Fixes

* correct database handling in jwt ([8782ebd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/8782ebdafdb634b2de1a4f1c6b81e83b7a4ed559))

## [7.2.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.6...v7.2.7) (2024-06-03)


### Bug Fixes

* use SMTP without starttls ([bfe58c8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/bfe58c82c04a861e3e3794f3c361cf261a71455d))

## [7.2.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.5...v7.2.6) (2024-06-03)


### Bug Fixes

* correct use of select in jwt service ([4d82e8d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/4d82e8d6587288c80dff3e8d1695f123e2295c6c))

## [7.2.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.4...v7.2.5) (2024-06-03)


### Bug Fixes

* do not send e-mail without recipients ([c919a9e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c919a9e5304d5f66a4705bba0da4dc25d63576da))
* generate MFA reset link correctly ([db470c6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/db470c6623f812cd963dffbea9f2643a9f2870fb))
* handle multi-value attributes correctly in user manager ([d233ff6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d233ff6f21e4efdc8bbe003793bf95d53874f102))

## [7.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.3...v7.2.4) (2024-06-03)


### Bug Fixes

* read mfa_reset_translations correctly ([8761dfb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/8761dfb4e304cb38aff96820f9a7df830b17d2bd))

## [7.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.2...v7.2.3) (2024-06-03)


### Bug Fixes

* correct MFA confirmation form ([325383a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/325383aee14f5de88286ae364619440ac6bca0c4))
* mfa_reset_jwt for gui.mfa_reset_verify ([75cf21e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/75cf21e6a6f7fa0a387e50ce94bba35b365c46f3))

## [7.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.1...v7.2.2) (2024-06-03)


### Bug Fixes

* **deps:** update dependency idpyoidc to ==2.1.0 ([38d13a3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/38d13a38a35aca06c4646cd2cdfec538dfdd817c))

## [7.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.2.0...v7.2.1) (2024-06-03)


### Bug Fixes

* deps in .commitlintrc.json ([b45a8d9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/b45a8d99e1ffba77e94cf0908662f29a22b62c73))

# [7.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.1.0...v7.2.0) (2024-05-31)


### Features

* mitre sessions logouts ([9741e8c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/9741e8c578e61c3959b06a9c265ab35051c766ba))

# [7.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v7.0.0...v7.1.0) (2024-05-29)


### Bug Fixes

* **heuristics:** heuristics page fixes ([cd6d75d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/cd6d75d196b407311bd63eeacda10c8877bb83ba))


### Features

* contrib document and new scopes ([fa1e83f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/fa1e83fd3a4d7211f8dc9db90e8daca09efeecb7))

# [7.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v6.0.2...v7.0.0) (2024-05-20)


### Reverts

* remove kerberos endpoint ([82ad1d5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/82ad1d5cf774916a78a211f0a38c3aa5b4530d8b))


### BREAKING CHANGES

* kerberos support dropped(for now)

## [6.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v6.0.1...v6.0.2) (2024-05-14)


### Bug Fixes

* missing line in bans input ([c90aef2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c90aef2b6032447514ffc5f9b218f3c6cb48150b))

## [6.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v6.0.0...v6.0.1) (2024-05-13)


### Bug Fixes

* correct ban propagation endpoint ([a5ab0f5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/a5ab0f500b8121967e646a6204a1a36fd7f6c749))

# [6.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.2.1...v6.0.0) (2024-05-13)


### Code Refactoring

* api consolidation ([c242d80](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c242d805e343e3aec9cae7d29ef6cb80a5393154))


### BREAKING CHANGES

* api endpoints are restructured

## [5.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.2.0...v5.2.1) (2024-05-10)


### Bug Fixes

* create ban table correctly ([d663d97](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d663d97d020719f67be39edcfedf03711c50e5f5))

# [5.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.1.4...v5.2.0) (2024-05-10)


### Features

* rewrite ban and jwt db to postgresql ([a112cbb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/a112cbb41025067a97478427e16139d74f9ac1ff))

## [5.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.1.3...v5.1.4) (2024-05-03)


### Bug Fixes

* heuristic page uses user_id instead of login ([3b07371](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/3b07371e1109991183e270575984764908e72a20))

## [5.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.1.2...v5.1.3) (2024-05-02)


### Bug Fixes

* heuristic_api tables creation ([4ab4425](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/4ab4425241a6bc6abd3fed2951e3a7cd0723c7c5))

## [5.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.1.1...v5.1.2) (2024-04-24)


### Bug Fixes

* name_id was missing in SAML logout request creation ([d9763c8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d9763c8fd0082b69421149dc55d1bbc2f1057c36))

## [5.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.1.0...v5.1.1) (2024-04-24)


### Bug Fixes

* redirect after option no on logout page ([834a135](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/834a135c07e128fa3a763f018bae458e5357ac8c))

# [5.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.0.2...v5.1.0) (2024-04-24)


### Features

* move auth_event db models to proxygui, with create check before calling ([14f2f37](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/14f2f37bfa0e92b0839c8978a6aa8ac8b7c75743))

## [5.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.0.1...v5.0.2) (2024-04-23)


### Bug Fixes

* heuristic api user cast to string ([7240842](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/72408428dc34ea36dbdb8780ec9e1b849bc58663))

## [5.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v5.0.0...v5.0.1) (2024-04-23)


### Bug Fixes

* configuration ([ac3f606](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/ac3f606fac90531efe803b0b13c75d89fa78b7a9))

# [5.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v4.1.1...v5.0.0) (2024-04-23)


### Features

* api splitted into optional parts ([34b3447](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/34b34475453d8f290657093d079152deac0ed5da))


### BREAKING CHANGES

* configuration file schema changed (see example config)

## [4.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v4.1.0...v4.1.1) (2024-04-23)


### Bug Fixes

* revert "feat: api splitted into optional parts" ([773bd96](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/773bd96c893895e8a7ac9646d93b65aede039685))

# [4.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v4.0.2...v4.1.0) (2024-04-22)


### Features

* api splitted into optional parts ([4958020](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/4958020d7ad26a53bd460b4b9720df6533a62e36))
* logout system readme docs ([9047b9a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/9047b9a2f09668f341a69aefc20bc449cbfd23ea))

## [4.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v4.0.1...v4.0.2) (2024-04-10)


### Bug Fixes

* handle null in session_indexes_detail ([920c081](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/920c081b143ec702df9da9ad09aa854fab4b6cd8))

## [4.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v4.0.0...v4.0.1) (2024-04-10)


### Bug Fixes

* correct look of submit button in HeuristicGetID ([48a1ff7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/48a1ff720c37f73d59170563f908980a17058416))
* remove redundant HTML from HeuristicGetID ([4734a1d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/4734a1d34a08d9ae64fc8953560976f7f8579d75))

# [4.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.5.1...v4.0.0) (2024-04-10)


### Features

* migration of SSP sessions to postgres ([fa72aa7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/fa72aa7952a5edf9a48cd7f72a36c354e8147f46))


### BREAKING CHANGES

* configuration from mongodb to postgres for ssp_db

## [3.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.5.0...v3.5.1) (2024-03-18)


### Bug Fixes

* allow tuples in config ([bdda7bd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/bdda7bdd9c65909823d1841c9ac6adaee76789ad))

# [3.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.4.2...v3.5.0) (2024-02-19)


### Features

* relocated api decumentation to openapi ([eb4ae24](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/eb4ae24d31b0375ff3d521ff401071283918c29b))

## [3.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.4.1...v3.4.2) (2024-02-15)


### Bug Fixes

* add scopes to auth request ([3531e4b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/3531e4ba702d787db44d1b6cf6027918e8df1257))

## [3.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.4.0...v3.4.1) (2024-02-01)


### Bug Fixes

* graceful oidc error handling ([c7ac2af](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c7ac2af6267dfcea929e5ae46ab477255197b9d1))

# [3.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.3.2...v3.4.0) (2024-01-25)


### Features

* semi-automated openapi ([f55dd3b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/f55dd3b763bd67db4ad5dbf3ee36f8fc456d6197))
* semi-automated openapi generation, modified some endpoint responses to be compatible ([13decc0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/13decc0b8473a56727e65deead55e1a2ed07a2b9))

## [3.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.3.1...v3.3.2) (2024-01-22)


### Bug Fixes

* authorization for heuristic endpoints ([b3a781d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/b3a781d24162a23f5c1b7c74542f36dc44e8e383))

## [3.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.3.0...v3.3.1) (2024-01-10)


### Bug Fixes

* add missing param to mfa reset ([e8b9f7c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/e8b9f7c8d69385d4995c777245c519358a25f64c))
* heuristic optional attribute handling ([1917eba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/1917ebaff6892889b62ca4bd409f541cdfefe8c5))
* heuristic sqlalchemy querying ([da8e1e2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/da8e1e22bae30dd0fddf99ed0d3de9c263b72e7b))
* redirect link in submit button ([4b9c9d9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/4b9c9d9eb22190b99fe4ac9c3ffcc323b6d4f2b4))

# [3.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.2.1...v3.3.0) (2024-01-03)


### Features

* gui endpoints for delegated mfa reset ([be143e1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/be143e11c04b452ad201d13cd893ca6e7e2890e4))

## [3.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.2.0...v3.2.1) (2024-01-03)


### Bug Fixes

* correct minor bugs in logout ([b208ef8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/b208ef820741f1820fcb82949f63dbdad8ca98f7))

# [3.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.1.0...v3.2.0) (2023-11-01)


### Features

* heuristic page for MFA reset ([33926ae](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/33926ae7a87076805f8dfbbdcc9a20d5d208fe91))

# [3.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.0.4...v3.1.0) (2023-10-12)


### Features

* logout system ([aab09e6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/aab09e6de6183bc1be4fbf147f1fd4db4d7db16f))

## [3.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.0.3...v3.0.4) (2023-10-02)


### Bug Fixes

* mfa reset page oidc auth ([644b04e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/644b04e681d87ee55e3965fcb9c7b0a662b9b2f9))

## [3.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.0.2...v3.0.3) (2023-09-20)


### Bug Fixes

* prevent double touple during introspection ([c7b40e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c7b40e7325c77359ec0c3c429a0f86b795c3c0fa))

## [3.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.0.1...v3.0.2) (2023-09-20)


### Bug Fixes

* correct metadata endpoint ([43072f8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/43072f828962c093e18ad54406a94062bc1fec9d))

## [3.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v3.0.0...v3.0.1) (2023-09-14)


### Bug Fixes

* optional attributes loading ([b33da1b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/b33da1b7d1d3bd60a8dc41166ffaf87774d030ca))

# [3.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v2.0.1...v3.0.0) (2023-09-11)


### Features

* mfa reset page ([5d54192](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/5d54192dc906b19531ce8d93df1edf081c76c80d))


### BREAKING CHANGES

* reworked oidc and jwt

## [2.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v2.0.0...v2.0.1) (2023-08-30)


### Bug Fixes

* import of resource protector in oauth ([d4b0915](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d4b091570b7ae380117cb7f5d8895b663a7f2a73))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.11.0...v2.0.0) (2023-08-29)


### Features

* extend consent management api ([5073ace](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/5073ace862ee3684d6cd1f30d7a877426ce1b5c6))


### BREAKING CHANGES

* `oauth2_provider` now required in config

# [1.11.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.10.0...v1.11.0) (2023-07-25)


### Features

* upgrade sqlalchemy usage to v2 ([9d51ca1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/9d51ca1e96229ec813b847c93fa2138b269b3918))

# [1.10.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.9.1...v1.10.0) (2023-07-12)


### Features

* sid logout support ([ead049d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/ead049d72ee9a6235819dea51876a9343f6d0e27))

## [1.9.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.9.0...v1.9.1) (2023-07-07)


### Bug Fixes

* show README on pypi.org ([5dda5cd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/5dda5cd85eccf7459bb12cfdaa3d63cfe785a89a))

# [1.9.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.8.4...v1.9.0) (2023-07-04)


### Features

* remove refresh tokens for banned users ([eda934b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/eda934ba863dc8bc42a069649bb95499251053ec))

## [1.8.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.8.3...v1.8.4) (2023-06-19)


### Bug Fixes

* fixed removing tokens from mitre ([59b3ead](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/59b3ead724f79cea82ee3a760c15103481fee2f4))

## [1.8.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.8.2...v1.8.3) (2023-06-15)


### Bug Fixes

* postgresql extra ([67d6cf6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/67d6cf6273d1a68d6faeba1ad6b5961447b21097))

## [1.8.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.8.1...v1.8.2) (2023-06-15)


### Bug Fixes

* correct loading of optional configs ([fa55336](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/fa55336fa9ff88c5dbadfd10f32ebfbb75c57f4f))

## [1.8.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.8.0...v1.8.1) (2023-06-14)


### Bug Fixes

* read global config from global folder ([8a40ff4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/8a40ff4ab40f3834f7154448ba61461bb4c1d4e4))

# [1.8.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.7.0...v1.8.0) (2023-06-06)


### Features

* backchannel_logout_endpoint ([8413ebc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/8413ebc530c92b54ce2d2106dc30797295e4e848))

# [1.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.6.1...v1.7.0) (2023-05-24)


### Bug Fixes

* minor changes ([0ab055d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/0ab055d353451dc78187108ebe13f4086962fd7c))
* tests ([20200ea](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/20200ea366a5756621350f40935188bb583fbb43))
* translations, tests, response handling ([cc366ee](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/cc366eead988a0b1172aa6b3b24d48d7963cff8a))


### Features

* consent framework, database and gui for registration ([fa293c0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/fa293c06256664e02cd0d8dc4eff666fd32fdf9b))

## [1.6.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.6.0...v1.6.1) (2023-05-23)


### Bug Fixes

* adapt to changes in flask-babel library ([f907874](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/f907874fd619eb2f901611ccdf5527ba8a706d65))

# [1.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.5.1...v1.6.0) (2023-04-12)


### Features

* ban endpoint accepts gzipped files, updates bans ([327cb2d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/327cb2d72fccb78e7b8829e9bc7e7be475a6c99e)), closes [#PRX-151](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/issues/PRX-151)

## [1.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.5.0...v1.5.1) (2023-03-16)


### Bug Fixes

* adjust how principal names attribute is obtained ([e634a89](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/e634a892f668239468edb9b35f74179a7eec0e05))

# [1.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.4.4...v1.5.0) (2023-02-16)


### Features

* config documentation ([630e58e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/630e58eda11bb4e2c6bff5fccc4a650db5e08911))

## [1.4.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.4.3...v1.4.4) (2023-02-15)


### Bug Fixes

* config now supports encodings other than ASCII ([e351f52](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/e351f52121a4b61b2eabfba880b1eb81889378be))

## [1.4.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.4.2...v1.4.3) (2023-02-13)


### Bug Fixes

* add kerberos dependencies to ci pipeline ([881f83b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/881f83b065ac79d7f9842df05f4d9d1523029d34))
* modify ci file to override existing configuration ([cdd950a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/cdd950a4dce2935f3625f1b797ff4f4291477354))
* move kerberos import to endpoint method body ([d3049ae](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d3049ae5f77eb0180d0150d2261a3ebbfcb409fc))
* remove unnecessary parts from override ([d6aa970](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d6aa970a932e66f1cbd87ff6d48f2597f2c4a28d))
* tidy formatting of new comment to comply with flake ([9ff1a58](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/9ff1a584b228fabece9b7c74d76aab48de507802))

## [1.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.4.1...v1.4.2) (2023-02-10)


### Bug Fixes

* correct get_app for uWSGI ([70d761a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/70d761a22cb71f0738b4f4de62b9807a24016e49))

## [1.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.4.0...v1.4.1) (2023-02-10)


### Bug Fixes

* callable by uWSGI ([cc76c93](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/cc76c93e089180c446c8cd09b011385957933986))

# [1.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.3.1...v1.4.0) (2023-02-10)


### Bug Fixes

* add black formatting to setup file ([692a81d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/692a81d2af4166e673407e064583e5e52f359240))
* define kerberos dependencies as extras ([c89b350](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/c89b350340178cf979f38081c302d9279ad92828))
* initiate negotiation when auth header is empty ([25a9dd6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/25a9dd65ee8512997756841dbcbefe67a59195ba))


### Features

* implement api for validating kerberos service tickets closes PROXYIDP-532 ([d753564](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/d753564bdf10a1ef095e5543b900575bedc7fd50))

## [1.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.3.0...v1.3.1) (2023-02-06)


### Bug Fixes

* pip install now include static, templates and translations ([3736e40](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/3736e40008dfffd8e71ac67ff47ad677d2c92332))

# [1.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.2.0...v1.3.0) (2023-01-24)


### Features

* implement api for banning users ([aa4c310](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/aa4c310b668757fbdeddaa77649c09198d6aaff3))

# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.1.1...v1.2.0) (2022-11-06)


### Features

* design update, color selection for bootstrap ([cecce56](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/cecce56d4c78f8cf63028c6c02c6eb747b8e6e6f))

## [1.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.1.0...v1.1.1) (2022-10-17)


### Bug Fixes

* correct folder structure and config location ([44d6a6d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/44d6a6d8e752deb5c9bea3e918d9be4252e32b4d))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.0.1...v1.1.0) (2022-10-17)


### Features

* tests ([33ed8c0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/33ed8c0f32a7e78a00671e9847942733fdd0353f))

## [1.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/compare/v1.0.0...v1.0.1) (2022-09-28)


### Bug Fixes

* correct order of semantic release plugins ([79105f7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/79105f7de6f26b4a88db1459880f6a9c89c35861))

# 1.0.0 (2022-09-28)


### Features

* workflow, requirements, setup, gitignore ([bdbf59c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/proxyidp-gui/commit/bdbf59cbf10238c3ab6b977e6c5e09bdc444426a))
