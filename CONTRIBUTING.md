# Contributing to Perun ProxyIdP GUI

## Table of Contents

[General guidelines](#general-guidelines)

[Commit Message Guidelines](#commit-message-guidelines)

## General guidelines

See general guidelines for [contributing to Perun AAI](https://gitlab.ics.muni.cz/perun/common/-/blob/main/CONTRIBUTING.md).

Additional rules are outlined in this document.

## Commit Message Guidelines

Use the name of the app as the scope of the commit message where applicable:

- bans
- consents
- heuristics
- logout
- mfa_reset

### Breaking Changes

Use `BREAKING CHANGE:`

- for new required configuration
    - e.g. new value is required or old value was renamed in `perun.proxygui.yaml`
    - configs can be found in the `config_templates` folder
- when functionality is removed
    - e.g. an API endpoint is removed or renamed
