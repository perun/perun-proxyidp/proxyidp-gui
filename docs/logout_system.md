# Logout system

The Logout system is a central point for managing user logouts. It provides a
convenient overview and logout options for Single Sign-On (SSO) ecosystems based on SAML
2.0 or OpenID Connect (OIDC). Users can see which services they are logged in and
have the option to log out of them.

Users can be automatically redirected to this point from other service providers (SPs)
after
logging out or visit the site directly to manually trigger log out from selected
services.

Users can choose between signing out of the services for the current session or all
of their services.

## Ways of initiating the logout

There are several ways of triggering the logout actions in the logout system. Logout
actions can be initiated from outside the system by calls from RPs or SPs or
directly within the system by a user request.
Logout actions are handled in `validate_request_and_extract_params` method of
`logout_manager.py`

### Relying Party (RP)

- RP-initiated logout happens when the user logs out from their RP, and it
  propagates the logout by calling central logout point

#### OIDC

- accepts parameters both in the query string and request body
- mandatory parameters
    - `id_token_hint`
- optional parameters
    - `post_logout_redirect_uri`
    - `client_id`
- meanings of parameters and more details about the logout process can be found in the
  [OIDC specification of RP-initiated logout](https://openid.net/specs/openid-connect-rpinitiated-1_0.html#RPLogout)
- if the `id_token_hint` is not valid, the request is considered invalid and
  [IdP-initiated flow](#identity-provider-idp) takes place
- if the token hint is valid, `client_id`, `post_logout_redirect_uri` and `state`
  are saved (if they are provided) and the message: _'You have been successfully logged
  out of RP'_ is shown to the user.

#### Alternative

- other alternative to OIDC capable of initiating the logout process and
  redirecting the user to the central logout point
- accepts parameters both in the query string and request body
- mandatory parameters
    - `logout_token`
- optional parameters
    - `client_id`
- meanings of parameters and more details about the logout process can be found in the
  [OIDC specification OIDC back-channel logout](https://openid.net/specs/openid-connect-backchannel-1_0.html#LogoutToken)

### Service Provider (SP)

#### SAML Single Logout (SLO)

- some SAML systems support both Single Sign-On (SSO) and Single Logout (SLO)
- SLO works oppositely to SSO, closing multiple sessions in a single operation
- **not implemented** in the logout system **yet**
- [SAML profiles specification (including Single Logout)](https://docs.oasis-open.org/security/saml/v2.0/saml-profiles-2.0-os.pdf)

### Identity Provider (IdP)

- user directly accesses the central logout point and requests a logout
  without previously being redirected by an RP or an SP
- the process is analogous to other flows, except that the message _‘You have been
  successfully logged out of RP’_ is not shown to the user

## Logout flows

The initiating RP/SP can pass a Session ID (SID) to the OP. If the OP receives an SID,
it will remove only the records related to the given SID, thus logging the user out of
the service on the OP side of the current session. If the RP/SP does not provide an SID,
the OP removes all records related to the initiating RP/SP, logging the user out of the
service on all devices.

### High-level view of logout flows

![logout flows_high_level](./images/logout_flows_high_level.png)

### Low-level view of logout flows

![logout flows_low_level](./images/logout_flows_low_level.webp)

## Types of logouts

ProxyIdP GUI supports several logout "protocols". These are represented by classes
derived from `LogoutRequest` in `perun/utils/logout_requests`. Each instance serves
to prepare a logout request to a single RP/SP. The requests, which require
information storage, are saved in a `mongoDB` database and rendered in an iframe.

### Back-Channel logout

- logout is performed by calling a dedicated endpoint of the RP
- handled using an iframe
- logout is resolved on the backend
- supports **both** logout from a **single** session or **all** the user's **sessions**
- Required configuration
    - `AUDIENCE`: [aud](<https://openid.net/specs/openid-connect-core-1_0.html#IDToken:~:text=case%2Dsensitive%20string.-,aud,-REQUIRED.%20Audience(s)>)
    claim of the logout token
    - `LOGOUT_ENDPOINT_URL`: URI of the dedicated Back-Channel logout endpoint
- [specification](https://openid.net/specs/openid-connect-backchannel-1_0.html)

### Front-Channel logout

- handled using front-channel communication without requiring an iframe
- logout is performed by calling a dedicated endpoint of the RP
- the result of the logout call is not guaranteed
- supports **both** logout from a **single** session or **all** the user's **sessions**
- Required configuration
    - `LOGOUT_URI`: URI of the dedicated logout endpoint
- [specification](https://openid.net/specs/openid-connect-frontchannel-1_0.html)

### Graph logout

- obtains an access token using the configured credentials and calls the
  `/revokeSignInSessions` endpoint of Graph API with `userPrincipalName` set to the
  current `sub`
- handled using an iframe
- logout is resolved on the backend
- supports **only** logout from **all** the user's **sessions**
- Required configuration
    - `TENANT_ID`: Microsoft tenant ID
    - `GRAPH_API_CLIENT_ID`: Microsoft identity platform app client ID
    - `GRAPH_API_CLIENT_SECRET`: Microsoft identity platform app client secret
- [specification](https://learn.microsoft.com/en-us/graph/api/user-revokesigninsessions?view=graph-rest-1.0&tabs=http)

### SAML logout

- handled using an iframe
- logout is resolved on the backend
- currently only supports HTTP-Redirect binding
- supports **both** logout from a **single** session or **all** the user's **sessions**
- Required configuration
    - `binding`: logout
    operation [binding](https://techdocs.broadcom.com/us/en/symantec-security-software/identity-security/siteminder/12-8/configuring/partnership-federation/logging-out-of-user-sessions/single-logout-overview-saml-2-0.html#:~:text=The%20following%20bindings%20are%20available%20for%20the%20single%20logout%20operation%3A)
    type (only HTTP-Redirect is currently supported)
    - `issuer_id`: identifier of the registered SAML callback endpoint
- [SAML profiles specification (including Single Logout)](https://docs.oasis-open.org/security/saml/v2.0/saml-profiles-2.0-os.pdf)
- [Single Logout Overview](https://techdocs.broadcom.com/us/en/symantec-security-software/identity-security/siteminder/12-8/configuring/partnership-federation/logging-out-of-user-sessions/single-logout-overview-saml-2-0.html)

## API Endpoints

`/logout-system`

- the base endpoint of the logout system
- it **must** be called with a cookie containing `SimpleSAMLSessionID`
- it presents two lists of services where the user is logged in
    - list of services connected to the current session
        - user can log out of these services using the _Log out_ button
    - list of all user's services retrieved from all the current sessions (on all
    devices) - user can log out of all the devices at once using the _Log out from devices_
    button

![logout_endpoint](./images/logout_endpoint.png)

`/logout-state`

- called from the `/logout` endpoint to prepare the logout requests selected by the
  user
- it removes the `SimpleSAMLSessionID` from the cookie
- iteratively triggers all the selected logout requests
    - **Front-channel logout**
        - direct call to the RP's configured logout URL
    - **Other logouts**
        - logout requests called in an iframe using the `/logout-iframe-callback`
      endpoint
- results of logout requests are presented on the final page in GUI

![logout-state_endpoint](./images/logout_state_endpoint.png)

`/logout-post`

- the last endpoint in the logout flow
- if `post_logout_redirect_uri` and request `state` are provided in the
  `logout_params`, user is redirected to the post logout uri and the `state` is
  displayed
- if no `post_logout_redirect_uri` is provided, user is redirected to a generic page
  confirming the logout

![post_logout_endpoint](./images/post_logout_endpoint.png)
